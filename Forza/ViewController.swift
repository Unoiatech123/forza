//
//  ViewController.swift
//  Forza
//
//  Created by Unoia Technologies on 24/10/18.
//  Copyright © 2018 Abc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //PMARK: Properties
    var headerView :  UIView!
    var splashText = UILabel()
    var splashImage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        setupView()
        setupSplashScreen()
        
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.splashTimeOut(sender:)), userInfo: nil, repeats: false)
    }

    func setupView(){
        headerView = UIView()
        headerView.backgroundColor = .white
        self.view.addSubview(headerView)
        
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        
    }
    
    func setupSplashScreen(){
        splashText.text = "Forza"
        splashText.font = UIFont.boldSystemFont(ofSize: 35)
        splashText.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(splashText)
        splashText.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        splashText.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
//        splashImage.image = #imageLiteral(resourceName: "logo")
//        splashImage.contentMode = .scaleAspectFit
//        view.addSubview(splashImage)
//
//        splashImage.translatesAutoresizingMaskIntoConstraints = false
//        splashImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        splashImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        splashImage.widthAnchor.constraint(equalToConstant: 200)
//        splashImage.heightAnchor.constraint(equalToConstant: 200)
        
    }
    
    @objc func splashTimeOut(sender : Timer){
        splashText.removeFromSuperview()
        splashImage.removeFromSuperview()
        showHomeScreen()
    }
    
    func showHomeScreen(){
        headerView.backgroundColor = .black
        let label = UILabel()
        label.text = "Welcome"
        label.font = UIFont.boldSystemFont(ofSize: 35)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
    }

    
}

